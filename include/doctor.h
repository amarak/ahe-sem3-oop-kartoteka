#include <iostream>
#include <string>
#include <person.h>

class Doctor : public Person{
	std::string  specializations;
    std::string work_hours;
    public:
        Doctor(
            std::string n= {}, 
            std::string s= {}, 
            int a={}, 
            std::string date={}, 
            std::string sp={}, 
            std::string h={}
            )
        : Person(n, s, a, date), specializations(sp), work_hours(h){};
        ~Doctor() {};
        std::string getSpecializations();
		void setSpecializations( std::string sp);
        std::string getWork_hours();
		void setWork_hours( std::string h);
};
