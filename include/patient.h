#include <iostream>
#include <person.h>
#include <string>

class Patient : public Person{
	std::string diseases;
	public:
		Patient(
            std::string n={}, 
            std::string s={}, 
            int a={}, 
            std::string date={}, 
            std::string d={}
            )
		: Person(n, s, a, date), diseases(d) {};
        ~Patient(){};
        std::string getDiseases();
		void setDiseases( std::string d);
};
