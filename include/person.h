#include <iostream>
#include <string>

#ifndef PERSON_HEADER_H
#define PERSON_HEADER_H

class Person{
	protected:
		std::string name;
		std::string surname;
		int age;
		std::string registration_date;
	public:
		Person( const std::string& n = {}, const std::string& s = {}, int a = {}, const std::string& d = {})
		: name(n), surname(s), age(a), registration_date(d){}
        virtual ~Person() {};
        const std::string &getName() const;
        void setName(const std::string &n);
        const std::string &getSurname() const;
        void setSurname(const std::string &s);
        int getAge() const;
        void setAge(int a);
        const std::string &getRegistrationDate() const;
        void setRegistrationDate(const std::string &d);
};

#endif