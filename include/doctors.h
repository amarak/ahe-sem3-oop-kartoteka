#include <iostream>
#include <doctor.h>
#include <vector>
class Doctors{
    std::vector<Doctor> docs_list;
    public:
    Doctors(){};
    ~Doctors(){};
    auto addDoctor();
    void deleteDoctor();
    auto findDoctor();
    auto editDoctor();
    void menu();
};