#include <iostream>
#include <patient.h>
#include <vector>
class Patients{
    std::vector<Patient> pat_list;
    public:
    Patients(){};
    ~Patients(){};
    auto addPatient();
    void deletePatient();
    auto findPatient();
    auto editPatient();
    void menu();
};