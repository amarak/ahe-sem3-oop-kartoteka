#include <iostream>
#include <doctor.h>

void Doctor::setSpecializations (std::string sp) {
	specializations = sp;
}

std::string Doctor::getSpecializations() {
	return this->specializations;
}

void Doctor::setWork_hours (std::string h) {
	work_hours = h;
}

std::string Doctor::getWork_hours() {
	return this->work_hours;
}