#include <iostream>
#include <person.h>

void Person::setName(const std::string &n) {
    name = n;
}

const std::string &Person::getSurname() const {
    return this->surname;
}

void Person::setSurname(const std::string &s) {
    surname = s;
}

int Person::getAge() const {
    return this->age;
}

const std::string &Person::getRegistrationDate() const {
    return this->registration_date;
}

void Person::setRegistrationDate(const std::string &d) {
    registration_date = d;
}

void Person::setAge(int a) {
    age = a;
}

const std::string &Person::getName() const {
    return this->name;
}
