#include <iostream>
#include <doctors.h>
#include <algorithm>
#include <vector>
#include <cstdio>

auto Doctors::addDoctor()
{
    std::string name;
    std::cout << "Podaj imie lekarza: ";
    std::cin >> name;
    std::string surname;
    std::cout << "Podaj nazwisko lekarza: ";
    std::cin >> surname;
    int age;
    std::cout << "Podaj wiek lekarza: ";
    std::cin >> age;
    std::string registration_date;
    std::cout << "Podaj date rejestracji lekarza: ";
    std::cin >> registration_date;
    std::string specializations;
    std::cout << "Podaj specjalizacje lekarza: ";
    std::cin >> specializations;
    std::string work_hours;
    std::cout << "Podaj godziny pracy lekarza: ";
    std::cin >> work_hours;
    docs_list.push_back({name, surname, age, registration_date, specializations, work_hours});
}
void Doctors::deleteDoctor()
{
    std::string surname;
    std::cout << "Podaj nazwisko lekarza, ktorego chcesz usunac: ";
    std::cin >> surname;
    docs_list.erase(std::remove_if(docs_list.begin(), docs_list.end(), [surname](const Doctor &a) { return a.getSurname() == surname; }),
                    docs_list.end());
}

auto Doctors::findDoctor()
{
    std::string surname;
    std::cout << "Podaj nazwisko lekarza, ktorego szukasz: ";
    std::cin >> surname;
    auto f = std::find_if(docs_list.begin(), docs_list.end(), [surname](const Doctor &a) { return a.getSurname() == surname; });
    if (f != docs_list.end())
    {
        std::cout << "1.Imie: " << f->getName() << "\n"
                  << "2.Nazwisko: " << f->getSurname() << "\n"
                  << "3.Wiek: " << f->getAge() << "\n"
                  << "4.Data Rejestracji: " << f->getRegistrationDate() << "\n"
                  << "5.Specjalizacja: " << f->getSpecializations() << "\n"
                  << "6.Godziny pracy: " << f->getWork_hours() << "\n"
                  << std::endl;
        return f;
    }
    else
    {
        std::cout << "nie ma takiego lekarza" << std::endl;
        return findDoctor();
    }
}
auto Doctors::editDoctor()
{
    auto f = findDoctor();
    if (f != docs_list.end())
    {
        int nr;
        do
        {
            std::string edit;
            std::cout << "Podaj nr i nowe zmiany lub 0 0 by wyjść" << std::endl;
            std::cin >> nr >> edit;
            int i = atoi(edit.c_str());
            switch (nr)
            {
            case 1:
                f->setName(edit);
                break;
            case 2:
                f->setSurname(edit);
                break;
            case 3:
                f->setAge(i);
                break;
            case 4:
                f->setRegistrationDate(edit);
                break;
            case 5:
                f->setSpecializations(edit);
                break;
            case 6:
                f->setWork_hours(edit);
                break;
            }
            std::cout << "lista po zmianie: "
                      << "\n"
                      << f->getName() << " " << f->getSurname() << " " << f->getAge() << " "
                      << f->getRegistrationDate() << " " << f->getSpecializations() << " "
                      << f->getWork_hours() << " " << std::endl;
        } while (nr != 0);
    }
}
void Doctors::menu()
{
    int nr;
    do
    {
        std::cout << "1. Dodaj Lekarza"
                  << "\n"
                  << "2. Usun Lekarza"
                  << "\n"
                  << "3. Edytuj"
                  << "\n"
                  << "4. Wyszukaj"
                  << "\n"
                  << "5. Zamknij program"
                  << "\n"
                  << "Wybierz numer dzialania: " << std::endl;
        std::cin >> nr;
        switch (nr)
        {
        case 1:
            this->addDoctor();
            break;
        case 2:
            this->deleteDoctor();
            break;
        case 3:
            this->editDoctor();
            break;
        case 4:
            this->findDoctor();
            break;
        case 5:
            std::cout << "";
            break;
        default:
            std::cout << "nie ma takiego numeru" << std::endl;
            break;
        }
    } while (nr != 5);
}