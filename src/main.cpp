//Wybralam kontener vector gdyz zalezalo mi aby miec szybki
// dostep do danych elementow kontenera oraz zeby miec mozliwosc
//realokacji pamieci gdy jakakolwiek metoda szablonu bedzie
//musiala zwiekszyc rozmiar tablicy kontenera.

#include <doctors.h>
#include <patients.h>

#include <iostream>
#include <memory>
int main()
{
    int nr;
    auto doctors_list = new Doctors();
    auto patiens_list = new Patients();

    do
    {
        std::cout << "1. Doktorzy"
                  << "\n"
                  << "2. Pacjenci"
                  << "\n"
                  << "3. Zamknij program"
                  << "\n"
                  << "Wybierz numer dzialania: " << std::endl;
        std::cin >> nr;
        switch (nr)
        {
        case 1:
            doctors_list->menu();
            break;
        case 2:
            patiens_list->menu();
            break;
        case 3:
            std::cout << "bye bye";
            break;
        default:
            std::cout << "nie ma takiego numeru" << std::endl;
            break;
        }
    } while (nr != 3);

    return 0;
}