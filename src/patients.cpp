#include <iostream>
#include <algorithm>
#include <vector>
#include <cstdio>
#include <patients.h>

auto Patients::addPatient()
{
    std::string name;
    std::cout << "Podaj imie pacjenta: ";
    std::cin >> name;
    std::string surname;
    std::cout << "Podaj nazwisko pacjenta: ";
    std::cin >> surname;
    int age;
    std::cout << "Podaj wiek pacjenta: ";
    std::cin >> age;
    std::string registration_date;
    std::cout << "Podaj date rejestracji pacjenta: ";
    std::cin >> registration_date;
    std::string diseases;
    std::cout << "Podaj liste chorob: ";
    std::cin >> diseases;
    pat_list.push_back({name, surname, age, registration_date, diseases});
}
void Patients::deletePatient()
{
    std::string surname;
    std::cout << "Podaj nazwisko pacjenta, ktorego chcesz usunac: ";
    std::cin >> surname;
    pat_list.erase(std::remove_if(pat_list.begin(), pat_list.end(), [&surname](const Patient &a) { return a.getSurname() == surname; }),
                   pat_list.end());
}

auto Patients::findPatient()
{
    std::string surname;
    std::cout << "Podaj nazwisko pacjenta, ktorego szukasz: ";
    std::cin >> surname;
    auto f = std::find_if(pat_list.begin(), pat_list.end(), [&surname](const Patient &a) { return a.getSurname() == surname; });
    if (f != pat_list.end())
    {
        std::cout << "1.Imie: " << f->getName() << "\n"
                  << "2.Nazwisko: " << f->getSurname() << "\n"
                  << "3.Wiek: " << f->getAge() << "\n"
                  << "4.Data Rejestracji: " << f->getRegistrationDate() << "\n"
                  << "5.Choroby: " << f->getDiseases() << std::endl;
        return f;
    }
    else
    {
        std::cout << "nie ma takiego pacjenta" << std::endl;
        return findPatient();
    }
}
auto Patients::editPatient()
{
    auto f = findPatient();
    if (f != pat_list.end())
    {
        int nr;
        do
        {
            std::string edit;
            std::cout << "Podaj nr i nowe zmiany lub 0 0 by wyjść" << std::endl;
            std::cin >> nr >> edit;
            int i = atoi(edit.c_str());
            switch (nr)
            {
            case 1:
                f->setName(edit);
                break;
            case 2:
                f->setSurname(edit);
                break;
            case 3:
                f->setAge(i);
                break;
            case 4:
                f->setRegistrationDate(edit);
                break;
            case 5:
                f->setDiseases(edit);
                break;
            }
            std::cout << "lista po zmianie: "
                      << "\n"
                      << f->getName() << " " << f->getSurname() << " " << f->getAge() << " "
                      << f->getRegistrationDate() << " " << f->getDiseases() << std::endl;
        } while (nr != 0);
    }
}
void Patients::menu()
{
    int nr;
    do
    {
        std::cout << "1. Dodaj Pacjenta"
                  << "\n"
                  << "2. Usun Pacjenta"
                  << "\n"
                  << "3. Edytuj"
                  << "\n"
                  << "4. Wyszukaj"
                  << "\n"
                  << "5. Zamknij program"
                  << "\n"
                  << "Wybierz numer dzialania: " << std::endl;
        std::cin >> nr;
        switch (nr)
        {
        case 1:
            // Patient newPatient;
            this->addPatient();
            break;
        case 2:
            this->deletePatient();
            break;
        case 3:
            this->editPatient();
            break;
        case 4:
            this->findPatient();
            break;
        case 5:
            std::cout << "";
            break;
        default:
            std::cout << "nie ma takiego numeru" << std::endl;
            break;
        }
    } while (nr != 5);
}